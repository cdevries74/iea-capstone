# Builds a URL and creates a post to Wordpress API
# Fall 2019

import requests
import json
import base64
# import getpass
import argparse
# from urllib.request import urlopen
# import html2text
import sys
import os

wordpress_username = os.getenv('WORDPRESS_USERNAME')
wordpress_password = os.getenv('WORDPRESS_PASSWORD')
ip = os.getenv('WORDPRESS_URL')

url = ''
headers = ''

option_text = 'Read latest post from the blog, or upload a blog post'


def build_url(ip):
    url = 'http://' + ip + ':8088'
    return url


def build_headers():
    tokenstring = wordpress_username + ':' + wordpress_password
    token = base64.b64encode(tokenstring.encode())
    headers = {'Authorization': 'Basic ' + token.decode('utf-8')}
    return headers


def readfile(infile):
    with open(infile) as f:
        title = f.readline()
        content = f.read()
    return title, content

# Putting the execution code for the progam below dunder main prevents
# the code from being executed when the test_pyblog file is run


if __name__ == '__main__':
    # Initiate the parser
    parser = argparse.ArgumentParser()
    parser.add_argument('option', choices=['read', 'upload'], help=option_text)
    parser.add_argument('-f', dest='infile', metavar='INPUT_FILE', help='Blog post input file')

    # Read arguments from the command line
    args = parser.parse_args()
    infile = args.infile

    # Check for arguments read, upload, or none defined
    # python3 pyblog.py read -- queries Wordpress API,
    # parses JSON reply, prints latest post
    # python3 pyblog.py -h read -- help file for read argument,
    # set through argparse
    if args.option == 'read':
        # Build URL & headers
        url = build_url(ip)
        headers = build_headers()
        wp_url = url + '/wp-json/wp/v2/posts'

        # Query API using get to retrieve json of posts
        r = requests.get(wp_url, headers=headers)

        # Parse output, print title and body of the newest post
        res = r.json()
        content_title = res[0].get('title').get('rendered')
        content_body = res[0].get('content').get('rendered')
        # content_body = html2text.html2text(content_body)
        print(content_title)
        print(content_body)
    elif args.option == 'upload':
        if args.infile is None:
            print("Error: upload requires -f <filename>")
        else:
            # Run readfile function to parse the input file to variables
            try:
                title, content = readfile(infile)
            except Exception:
                sys.exit(print('Error: Invalid file name'))

            # Use the variables to build the json to be posted,
            # assign to a variable
            # Wordpress requires title, content, or excerpt keys
            # in the API post. All others are optional
            post = {
                'title': title,
                'slug': 'rest-api-1',
                'status': 'publish',
                'content': content,
                'author': '1',
                'excerpt': 'This is the best',
                'format': 'standard'
                }

            # post the contents
            # Build the URL and headers
            url = build_url(ip)
            wp_url = url + '/wp-json/wp/v2/posts'
            headers = build_headers()
            # Post to the Wordpress API
            try:
                r = requests.post(wp_url, headers=headers, json=post)
                # Output with link
                print('Posted successfully to ' + json.loads(r.content.decode('utf-8'))['link'])
            except Exception:
                print("Error creating post. Validate username and password.")
