# UNIT TEST
# Tests API to obtain location based on user input
# Tests
# Tests

import requests
import unittest
import os
from requests.auth import HTTPBasicAuth
from pyblog import build_url

class TestValidateWordpressUp(unittest.TestCase):

    def test_wordpress_URL_from_good_IP(self):
        ip = '35.174.172.169'
        url = build_url(ip)
        res = requests.get(url, timeout=5)
        self.assertEqual(res.status_code, 200)

    def test_bad_User_Password(self):
        ip = '35.174.172.169'
        url = build_url(ip)
        wp_url = url + '/wp-json/wp/v2/posts'
        user_name = 'abcde'
        user_password = '1234'
        res = requests.get(wp_url, auth=HTTPBasicAuth(user_name, user_password), headers={"Accept": "application/json"})
        self.assertEqual(res.status_code,401)
        
    def test_no_input_file(self):
        infile = ''
        self.assertFalse(os.path.exists('file.txt'))

if __name__ == '__main__':
    unittest.main()
