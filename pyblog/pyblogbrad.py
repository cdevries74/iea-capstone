import requests
import json
import base64
import argparse
import sys
from requests.auth import HTTPBasicAuth

url = "http://52.207.137.110:8088/wp-json/wp/v2/posts"

# what to post for the blog output
post = {'date': '2019-11-04T20:00:35',
        'title': 'Brads Fikkrst REST API post',
        'slug': 'rest-api-1',
        'status': 'publish',
        'content': 'this isdf brads new content post',
        'author': '1',
        'excerpt': 'Exceptiokjknal post!',
        'format': 'standard'
        }


def _exit(code, message):
    ''' terminate program using provided code after displaying error message'''
    print(message)
    exit(code)

# check username and password to see if valid
def valid_User_Password(user_name, user_password):
    """
    Function to check whether the username and password are valid for logging into wordpress
    :param user_name: (A string value)
    :param user_password: (A string value)
    :return True if valid, False otherwise.
    """
    return_value = True

    if user_name is None:
        _exit(-1, "No username specified.  Please enter a username")

    if user_password is None:
        _exit(-1, "No password specified.  Please enter a password")

    try:
        requestOutput = requests.get(url, auth=(user_name, user_password))
        data = requestOutput.json()

        if requestOutput.status_code == 401:
            print("Unauthorized login.  Please try again")
            return_value = False

    except Exception as error:
        print(str(error))
        return_value= False

    return return_value

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-username',
                        nargs=1,
                        dest="user_name",
                        required=True,
                        help='Enter a username')

    parser.add_argument('-password',
                        nargs=1,
                        dest="user_password",
                        required=True,
                        help='Enter a password')

    args = parser.parse_args()
    user_name = str(args.user_name)
    user_password = str(args.user_password)

    valid_User_Password(user_name,user_password)
