FROM python:3.7-alpine

COPY ./pyblog /app

WORKDIR /app

RUN pip3 install --no-cache-dir -r /app/requirements.txt

ENV WORDPRESS_USERNAME=bamboo
ENV WORDPRESS_PASSWORD=bamboo-admin
ENV WORDPRESS_URL=35.174.172.169

ENTRYPOINT [ "python3", "pyblog.py" ]